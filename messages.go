package mainway

import (
	"encoding/json"
	"io"
	"log"
)

type Message struct {
	Message string `json:"message"`
}

func NewMessage(message string) *Message {
	return &Message{
		Message: message,
	}
}

func (message *Message) Write(w io.Writer) {
	if err := json.NewEncoder(w).Encode(message); err != nil {
		log.Println(err)
	}
}
