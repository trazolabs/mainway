package forwarder

import (
	"log"
	"net/http"
	"net/url"
)

type Prefix struct {
	Prefix string
}

func NewPrefix(prefix string) *Prefix {
	return &Prefix{
		Prefix: prefix,
	}
}

func (handler *Prefix) Proxy(r *http.Request) (*http.Response, error) {
	target, err := url.Parse(handler.Prefix)
	if err != nil {
		return nil, err
	}

	r.URL.Host = target.Host
	r.URL.Scheme = target.Scheme
	r.Host = target.Host

	response, err := http.DefaultTransport.RoundTrip(r)
	if err != nil {
		log.Println(r.Method, r.URL.Path, "--", err.Error())
		return nil, err
	}

	log.Println(r.Method, r.URL.Path, "--", response.Status)

	return response, err
}

func (handler *Prefix) IsAuthorized(r *http.Request) bool {
	return true
}
