package forwarder

import (
	"log"
	"net/http"
	"net/url"
)

type Replace struct {
	To string
}

func NewReplace(to string) *Replace {
	return &Replace{
		To: to,
	}
}

func (handler *Replace) Proxy(r *http.Request) (*http.Response, error) {
	target, err := url.Parse(handler.To)
	if err != nil {
		return nil, err
	}

	r.URL.Host = target.Host
	r.URL.Scheme = target.Scheme
	r.Host = target.Host
	r.URL.Path = target.Path
	r.URL.RawPath = target.RawPath

	response, err := http.DefaultTransport.RoundTrip(r)
	if err != nil {
		log.Println(r.Method, r.URL.Path, "--", err.Error())
		return nil, err
	}

	log.Println(r.Method, r.URL.Path, "--", response.Status)

	return response, err
}

func (handler *Replace) IsAuthorized(r *http.Request) bool {
	return true
}
