package mainway

type Method struct {
	Authorizer Authorizer
}

type Route struct {
	Path string

	Forwarder Forwarder

	Get    Authorizer
	Post   Authorizer
	Put    Authorizer
	Delete Authorizer
	Patch  Authorizer
}

type Builder []Route

func (builder Builder) Build(gateway *Gateway) {
	for _, route := range builder {
		if route.Get != nil {
			gateway.Handle("GET", route.Path, route.Forwarder, route.Get)
		}

		if route.Post != nil {
			gateway.Handle("POST", route.Path, route.Forwarder, route.Post)
		}

		if route.Put != nil {
			gateway.Handle("PUT", route.Path, route.Forwarder, route.Put)
		}

		if route.Patch != nil {
			gateway.Handle("PATCH", route.Path, route.Forwarder, route.Patch)
		}

		if route.Delete != nil {
			gateway.Handle("DELETE", route.Path, route.Forwarder, route.Delete)
		}
	}
}
