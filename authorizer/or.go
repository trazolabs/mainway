package authorizer

import (
	"gitlab.com/trazolabs/mainway"
	"net/http"
)

// Or checks if any authorizer gives the requested permission.
type Or struct {
	authorizers []mainway.Authorizer
}

func NewOr(authorizers ...mainway.Authorizer) mainway.Authorizer {
	return &Or{
		authorizers: authorizers,
	}
}

func (authorizer *Or) IsAuthorized(r *http.Request) bool {
	for _, authorizer := range authorizer.authorizers {
		if authorized := authorizer.IsAuthorized(r); authorized {
			return true
		}
	}

	return false
}
