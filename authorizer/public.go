package authorizer

import (
	"gitlab.com/trazolabs/mainway"
	"net/http"
)

type Public struct {
}

func NewPublic() mainway.Authorizer {
	return &Public{}
}

func (authorizer *Public) IsAuthorized(r *http.Request) bool {
	return true
}
