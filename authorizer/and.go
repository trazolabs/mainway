package authorizer

import (
	"gitlab.com/trazolabs/mainway"
	"net/http"
)

// Or checks if any authorizer gives the requested permission.
type And struct {
	authorizers []mainway.Authorizer
}

func NewAnd(authorizers ...mainway.Authorizer) mainway.Authorizer {
	return &And{
		authorizers: authorizers,
	}
}

func (authorizer *And) IsAuthorized(r *http.Request) bool {
	for _, authorizer := range authorizer.authorizers {
		if authorized := authorizer.IsAuthorized(r); !authorized {
			return false
		}
	}
	return true
}
