package mainway

import (
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"io"
	"log"
	"net/http"
)

const CatchRest = `{rest:[a-zA-Z0-9=\-\/]+}`

type Gateway struct {
	router *mux.Router
}

func NewGateway() *Gateway {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	return &Gateway{
		router: mux.NewRouter(),
	}
}

func GenericHandler(authorizer Authorizer, handler Forwarder) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.Method, r.URL.Path, "processing...")

		// Check authorization.
		if isAuthorized := authorizer.IsAuthorized(r); !isAuthorized {
			log.Println(r.Method, r.URL.Path, "not authorized")
			w.WriteHeader(403)
			NewMessage("unauthorized").Write(w)
			return
		}

		// Make request.
		response, err := handler.Proxy(r)
		if err != nil {
			log.Printf("%+v\n", err)
			w.WriteHeader(502)
			NewMessage("error communicating with the requested service").Write(w)
			return
		}

		// Write response.
		defer response.Body.Close()

		// copy header.
		for name, values := range response.Header {
			w.Header().Set(name, values[0])
		}
		w.WriteHeader(response.StatusCode)

		// write response to body.
		if _, err := io.Copy(w, response.Body); err != nil {
			log.Printf("%+v\n", err)
			w.WriteHeader(502)
			NewMessage("error writing response").Write(w)
			return
		}
	}
}

func (gateway *Gateway) Handle(method, route string, forwarder Forwarder, authorizer Authorizer) {
	gateway.router.
		Methods(method).
		Path(route).HandlerFunc(GenericHandler(authorizer, forwarder))
}

func (gateway *Gateway) Serve(addr string) error {
	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "POST", "PUT", "HEAD", "OPTIONS", "DELETE"})

	return http.ListenAndServe(addr, handlers.CORS(headersOk, methodsOk, originsOk)(gateway.router))
}
