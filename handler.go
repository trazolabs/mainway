package mainway

import "net/http"

type Forwarder interface {
	Proxy(r *http.Request) (*http.Response, error)
}
